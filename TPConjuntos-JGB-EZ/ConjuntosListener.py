# Generated from Conjuntos.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .ConjuntosParser import ConjuntosParser
else:
    from ConjuntosParser import ConjuntosParser

# This class defines a complete listener for a parse tree produced by ConjuntosParser.
class ConjuntosListener(ParseTreeListener):

    # Enter a parse tree produced by ConjuntosParser#program.
    def enterProgram(self, ctx:ConjuntosParser.ProgramContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#program.
    def exitProgram(self, ctx:ConjuntosParser.ProgramContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#statement.
    def enterStatement(self, ctx:ConjuntosParser.StatementContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#statement.
    def exitStatement(self, ctx:ConjuntosParser.StatementContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#new_set.
    def enterNew_set(self, ctx:ConjuntosParser.New_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#new_set.
    def exitNew_set(self, ctx:ConjuntosParser.New_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#add_set.
    def enterAdd_set(self, ctx:ConjuntosParser.Add_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#add_set.
    def exitAdd_set(self, ctx:ConjuntosParser.Add_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#remove_set.
    def enterRemove_set(self, ctx:ConjuntosParser.Remove_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#remove_set.
    def exitRemove_set(self, ctx:ConjuntosParser.Remove_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#union_set.
    def enterUnion_set(self, ctx:ConjuntosParser.Union_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#union_set.
    def exitUnion_set(self, ctx:ConjuntosParser.Union_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#intersect_set.
    def enterIntersect_set(self, ctx:ConjuntosParser.Intersect_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#intersect_set.
    def exitIntersect_set(self, ctx:ConjuntosParser.Intersect_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#complement_set.
    def enterComplement_set(self, ctx:ConjuntosParser.Complement_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#complement_set.
    def exitComplement_set(self, ctx:ConjuntosParser.Complement_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#diff_set.
    def enterDiff_set(self, ctx:ConjuntosParser.Diff_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#diff_set.
    def exitDiff_set(self, ctx:ConjuntosParser.Diff_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#show_set.
    def enterShow_set(self, ctx:ConjuntosParser.Show_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#show_set.
    def exitShow_set(self, ctx:ConjuntosParser.Show_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#sum_set.
    def enterSum_set(self, ctx:ConjuntosParser.Sum_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#sum_set.
    def exitSum_set(self, ctx:ConjuntosParser.Sum_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#prom_set.
    def enterProm_set(self, ctx:ConjuntosParser.Prom_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#prom_set.
    def exitProm_set(self, ctx:ConjuntosParser.Prom_setContext):
        pass


    # Enter a parse tree produced by ConjuntosParser#num_set.
    def enterNum_set(self, ctx:ConjuntosParser.Num_setContext):
        pass

    # Exit a parse tree produced by ConjuntosParser#num_set.
    def exitNum_set(self, ctx:ConjuntosParser.Num_setContext):
        pass


