from antlr4 import CommonTokenStream, ParseTreeWalker, FileStream

from Seminario1Lexer import Seminario1Lexer
from Seminario1Parser import Seminario1Parser
from Seminario1Listener import Seminario1Listener


print('Comenzando...')
input = FileStream('codigo1.sem')
lexer = Seminario1Lexer(input)
stream = CommonTokenStream(lexer)
parser = Seminario1Parser(stream)

tree = parser.program()


print('Fin.')