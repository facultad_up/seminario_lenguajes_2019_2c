# Generated from Conjuntos.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\21")
        buf.write("a\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\3\3\3\3\4\3\4")
        buf.write("\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3")
        buf.write("\f\6\fC\n\f\r\f\16\fD\3\f\7\fH\n\f\f\f\16\fK\13\f\3\r")
        buf.write("\3\r\3\r\3\r\6\rQ\n\r\r\r\16\rR\5\rU\n\r\3\16\6\16X\n")
        buf.write("\16\r\16\16\16Y\3\17\3\17\3\20\3\20\3\20\3\20\2\2\21\3")
        buf.write("\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write("\33\17\35\20\37\21\3\2\6\4\2C\\c|\5\2\62;C\\c|\3\2\62")
        buf.write(";\5\2\13\f\17\17\"\"\2e\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3")
        buf.write("\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2")
        buf.write("\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2")
        buf.write("\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\3")
        buf.write("!\3\2\2\2\5$\3\2\2\2\7&\3\2\2\2\t(\3\2\2\2\13*\3\2\2\2")
        buf.write("\r,\3\2\2\2\17.\3\2\2\2\21\60\3\2\2\2\23\66\3\2\2\2\25")
        buf.write(";\3\2\2\2\27B\3\2\2\2\31T\3\2\2\2\33W\3\2\2\2\35[\3\2")
        buf.write("\2\2\37]\3\2\2\2!\"\7>\2\2\"#\7/\2\2#\4\3\2\2\2$%\7-\2")
        buf.write("\2%\6\3\2\2\2&\'\7t\2\2\'\b\3\2\2\2()\7w\2\2)\n\3\2\2")
        buf.write("\2*+\7p\2\2+\f\3\2\2\2,-\7#\2\2-\16\3\2\2\2./\7f\2\2/")
        buf.write("\20\3\2\2\2\60\61\7u\2\2\61\62\7j\2\2\62\63\7q\2\2\63")
        buf.write("\64\7y\2\2\64\65\7\"\2\2\65\22\3\2\2\2\66\67\7u\2\2\67")
        buf.write("8\7w\2\289\7o\2\29:\7\"\2\2:\24\3\2\2\2;<\7r\2\2<=\7t")
        buf.write("\2\2=>\7q\2\2>?\7o\2\2?@\7\"\2\2@\26\3\2\2\2AC\t\2\2\2")
        buf.write("BA\3\2\2\2CD\3\2\2\2DB\3\2\2\2DE\3\2\2\2EI\3\2\2\2FH\t")
        buf.write("\3\2\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2J\30\3\2")
        buf.write("\2\2KI\3\2\2\2LU\5\33\16\2MP\5\33\16\2NO\7.\2\2OQ\5\33")
        buf.write("\16\2PN\3\2\2\2QR\3\2\2\2RP\3\2\2\2RS\3\2\2\2SU\3\2\2")
        buf.write("\2TL\3\2\2\2TM\3\2\2\2U\32\3\2\2\2VX\5\35\17\2WV\3\2\2")
        buf.write("\2XY\3\2\2\2YW\3\2\2\2YZ\3\2\2\2Z\34\3\2\2\2[\\\t\4\2")
        buf.write("\2\\\36\3\2\2\2]^\t\5\2\2^_\3\2\2\2_`\b\20\2\2` \3\2\2")
        buf.write("\2\b\2DIRTY\3\b\2\2")
        return buf.getvalue()


class ConjuntosLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    VARIABLE = 11
    SETLIST = 12
    NUMBER = 13
    DIGIT = 14
    WS = 15

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'<-'", "'+'", "'r'", "'u'", "'n'", "'!'", "'d'", "'show '", 
            "'sum '", "'prom '" ]

    symbolicNames = [ "<INVALID>",
            "VARIABLE", "SETLIST", "NUMBER", "DIGIT", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "VARIABLE", "SETLIST", "NUMBER", 
                  "DIGIT", "WS" ]

    grammarFileName = "Conjuntos.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


