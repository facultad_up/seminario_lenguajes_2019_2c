# Generated from Aritmetica.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .AritmeticaParser import AritmeticaParser
else:
    from AritmeticaParser import AritmeticaParser

# This class defines a complete listener for a parse tree produced by AritmeticaParser.
class AritmeticaListener(ParseTreeListener):

    # Enter a parse tree produced by AritmeticaParser#program.
    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#program.
    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#statement.
    def enterStatement(self, ctx:AritmeticaParser.StatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#statement.
    def exitStatement(self, ctx:AritmeticaParser.StatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#for_statement.
    def enterFor_statement(self, ctx:AritmeticaParser.For_statementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#for_statement.
    def exitFor_statement(self, ctx:AritmeticaParser.For_statementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#while_statement.
    def enterWhile_statement(self, ctx:AritmeticaParser.While_statementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#while_statement.
    def exitWhile_statement(self, ctx:AritmeticaParser.While_statementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#if_statement.
    def enterIf_statement(self, ctx:AritmeticaParser.If_statementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#if_statement.
    def exitIf_statement(self, ctx:AritmeticaParser.If_statementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#assign_statement.
    def enterAssign_statement(self, ctx:AritmeticaParser.Assign_statementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#assign_statement.
    def exitAssign_statement(self, ctx:AritmeticaParser.Assign_statementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#booleanexpression.
    def enterBooleanexpression(self, ctx:AritmeticaParser.BooleanexpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#booleanexpression.
    def exitBooleanexpression(self, ctx:AritmeticaParser.BooleanexpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#expression.
    def enterExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#expression.
    def exitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#term.
    def enterTerm(self, ctx:AritmeticaParser.TermContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#term.
    def exitTerm(self, ctx:AritmeticaParser.TermContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#factor.
    def enterFactor(self, ctx:AritmeticaParser.FactorContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#factor.
    def exitFactor(self, ctx:AritmeticaParser.FactorContext):
        pass


