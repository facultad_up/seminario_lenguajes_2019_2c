# Seminario_lenguajes_2019_2c


## Preparación del entorno de trabajo

- Instalar Java 8+

- Instalar Python 3.6+

- Clonar el projecto:
    ```
    git clone https://gitlab.com/facultad_up/seminario_lenguajes_2019_2c.git
    ```
- Ir en la consola al directorio del proyecto. 

- Instalar el administrador de Entornos Virtuales de python:
    ```
    pip install virtualenv
    ```

- Crear el entorno virtual de trabajo:
    ```
    virtualenv env
    ```
  En el caso de tener instalado python 2.7 y 3, para forzar que el entorno virtual sea 3:
    ```
    virtualenv env -p python3
    ```

- Activar el entorno virtual:
    ```
    source env/Scripts/activate      En Windows con bash
    source env/bin/activate         En linux/mac
    .\env\Scripts\activate.bat       En windows con commad/powershell
    ```
- Instalar el paquete de ANTLR4
    ```
    pip install antlr4-python3-runtime
    ```
- Descargar el **jar** de ANTLR4 y grabarlo en el directorio del projecto:
    ```
    https://www.antlr.org/download/antlr-4.7.2-complete.jar
    ```
- Mantener la lista de paquetes con su version instalados:
    ```
    pip freeze > requeriments.txt
    ```

- Reinstalar los paquetes:
    ```
    pip install -r requeriments.txt
    ```

- En el visual studio code, agregar la extension para ANTLR4


## tree.bat

    Uso: tree.bat Gramatica regla archivoejemplo