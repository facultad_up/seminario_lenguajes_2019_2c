grammar Complex;

// en standby hasta recorrer arbol

// a+bi  a    bi

complex:
      NUMERO
    | NUMERO 'i' 
    | 'i'
    | '-' 'i'
    | NUMERO '+' NUMERO 'i'
    | NUMERO '-' NUMERO 'i'
    ;

NUMERO : [0-9]+;

WS : [ \r\n\t] -> skip;