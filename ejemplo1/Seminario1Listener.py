# Generated from Seminario1.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .Seminario1Parser import Seminario1Parser
else:
    from Seminario1Parser import Seminario1Parser

# This class defines a complete listener for a parse tree produced by Seminario1Parser.
class Seminario1Listener(ParseTreeListener):

    # Enter a parse tree produced by Seminario1Parser#program.
    def enterProgram(self, ctx:Seminario1Parser.ProgramContext):
        pass

    # Exit a parse tree produced by Seminario1Parser#program.
    def exitProgram(self, ctx:Seminario1Parser.ProgramContext):
        pass


    # Enter a parse tree produced by Seminario1Parser#statement.
    def enterStatement(self, ctx:Seminario1Parser.StatementContext):
        pass

    # Exit a parse tree produced by Seminario1Parser#statement.
    def exitStatement(self, ctx:Seminario1Parser.StatementContext):
        pass


    # Enter a parse tree produced by Seminario1Parser#regla_saludar.
    def enterRegla_saludar(self, ctx:Seminario1Parser.Regla_saludarContext):
        pass

    # Exit a parse tree produced by Seminario1Parser#regla_saludar.
    def exitRegla_saludar(self, ctx:Seminario1Parser.Regla_saludarContext):
        pass


    # Enter a parse tree produced by Seminario1Parser#reglar_insultar.
    def enterReglar_insultar(self, ctx:Seminario1Parser.Reglar_insultarContext):
        pass

    # Exit a parse tree produced by Seminario1Parser#reglar_insultar.
    def exitReglar_insultar(self, ctx:Seminario1Parser.Reglar_insultarContext):
        pass


