# Generated from Aritmetica.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\25")
        buf.write("l\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3\2\6\2\30\n\2\r\2\16\2")
        buf.write("\31\3\3\3\3\3\3\3\3\3\3\5\3!\n\3\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\4\6\4)\n\4\r\4\16\4*\3\4\3\4\3\5\3\5\3\5\3\5\6\5\63\n")
        buf.write("\5\r\5\16\5\64\3\5\3\5\3\6\3\6\3\6\3\6\6\6=\n\6\r\6\16")
        buf.write("\6>\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\t\3\t\7\tT\n\t\f\t\16\tW\13\t\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\n\7\n_\n\n\f\n\16\nb\13\n\3\13\3\13")
        buf.write("\3\13\3\13\3\13\3\13\5\13j\n\13\3\13\2\4\20\22\f\2\4\6")
        buf.write("\b\n\f\16\20\22\24\2\3\3\2\f\16\2n\2\27\3\2\2\2\4 \3\2")
        buf.write("\2\2\6\"\3\2\2\2\b.\3\2\2\2\n8\3\2\2\2\fB\3\2\2\2\16F")
        buf.write("\3\2\2\2\20J\3\2\2\2\22X\3\2\2\2\24i\3\2\2\2\26\30\5\4")
        buf.write("\3\2\27\26\3\2\2\2\30\31\3\2\2\2\31\27\3\2\2\2\31\32\3")
        buf.write("\2\2\2\32\3\3\2\2\2\33!\5\20\t\2\34!\5\n\6\2\35!\5\f\7")
        buf.write("\2\36!\5\b\5\2\37!\5\6\4\2 \33\3\2\2\2 \34\3\2\2\2 \35")
        buf.write("\3\2\2\2 \36\3\2\2\2 \37\3\2\2\2!\5\3\2\2\2\"#\7\3\2\2")
        buf.write("#$\7\23\2\2$%\7\4\2\2%&\7\23\2\2&(\7\5\2\2\')\5\4\3\2")
        buf.write("(\'\3\2\2\2)*\3\2\2\2*(\3\2\2\2*+\3\2\2\2+,\3\2\2\2,-")
        buf.write("\7\6\2\2-\7\3\2\2\2./\7\7\2\2/\60\5\16\b\2\60\62\7\5\2")
        buf.write("\2\61\63\5\4\3\2\62\61\3\2\2\2\63\64\3\2\2\2\64\62\3\2")
        buf.write("\2\2\64\65\3\2\2\2\65\66\3\2\2\2\66\67\7\6\2\2\67\t\3")
        buf.write("\2\2\289\7\b\2\29:\5\16\b\2:<\7\5\2\2;=\5\4\3\2<;\3\2")
        buf.write("\2\2=>\3\2\2\2><\3\2\2\2>?\3\2\2\2?@\3\2\2\2@A\7\6\2\2")
        buf.write("A\13\3\2\2\2BC\7\21\2\2CD\7\t\2\2DE\5\20\t\2E\r\3\2\2")
        buf.write("\2FG\5\20\t\2GH\7\22\2\2HI\5\20\t\2I\17\3\2\2\2JK\b\t")
        buf.write("\1\2KL\5\22\n\2LU\3\2\2\2MN\f\4\2\2NO\7\n\2\2OT\5\22\n")
        buf.write("\2PQ\f\3\2\2QR\7\13\2\2RT\5\22\n\2SM\3\2\2\2SP\3\2\2\2")
        buf.write("TW\3\2\2\2US\3\2\2\2UV\3\2\2\2V\21\3\2\2\2WU\3\2\2\2X")
        buf.write("Y\b\n\1\2YZ\5\24\13\2Z`\3\2\2\2[\\\f\3\2\2\\]\t\2\2\2")
        buf.write("]_\5\24\13\2^[\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2a")
        buf.write("\23\3\2\2\2b`\3\2\2\2cj\7\23\2\2dj\7\21\2\2ef\7\17\2\2")
        buf.write("fg\5\20\t\2gh\7\20\2\2hj\3\2\2\2ic\3\2\2\2id\3\2\2\2i")
        buf.write("e\3\2\2\2j\25\3\2\2\2\13\31 *\64>SU`i")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'from'", "'to'", "'?'", "'.?'", "'while'", 
                     "'..'", "'<-'", "'+'", "'-'", "'*'", "'/'", "'^'", 
                     "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "VARIABLE", 
                      "COMPARATION_OPERATOR", "NUMBER", "DIGIT", "WS" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_for_statement = 2
    RULE_while_statement = 3
    RULE_if_statement = 4
    RULE_assign_statement = 5
    RULE_booleanexpression = 6
    RULE_expression = 7
    RULE_term = 8
    RULE_factor = 9

    ruleNames =  [ "program", "statement", "for_statement", "while_statement", 
                   "if_statement", "assign_statement", "booleanexpression", 
                   "expression", "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    VARIABLE=15
    COMPARATION_OPERATOR=16
    NUMBER=17
    DIGIT=18
    WS=19

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = AritmeticaParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 20
                self.statement()
                self.state = 23 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__0) | (1 << AritmeticaParser.T__4) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def if_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.If_statementContext,0)


        def assign_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.Assign_statementContext,0)


        def while_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.While_statementContext,0)


        def for_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.For_statementContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 30
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 25
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 26
                self.if_statement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 27
                self.assign_statement()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 28
                self.while_statement()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 29
                self.for_statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class For_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.a = None # Token
            self.b = None # Token

        def NUMBER(self, i:int=None):
            if i is None:
                return self.getTokens(AritmeticaParser.NUMBER)
            else:
                return self.getToken(AritmeticaParser.NUMBER, i)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_for_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFor_statement" ):
                listener.enterFor_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFor_statement" ):
                listener.exitFor_statement(self)




    def for_statement(self):

        localctx = AritmeticaParser.For_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_for_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 32
            self.match(AritmeticaParser.T__0)
            self.state = 33
            localctx.a = self.match(AritmeticaParser.NUMBER)
            self.state = 34
            self.match(AritmeticaParser.T__1)
            self.state = 35
            localctx.b = self.match(AritmeticaParser.NUMBER)
            self.state = 36
            self.match(AritmeticaParser.T__2)
            self.state = 38 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 37
                self.statement()
                self.state = 40 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__0) | (1 << AritmeticaParser.T__4) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 42
            self.match(AritmeticaParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class While_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def booleanexpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BooleanexpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_while_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhile_statement" ):
                listener.enterWhile_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhile_statement" ):
                listener.exitWhile_statement(self)




    def while_statement(self):

        localctx = AritmeticaParser.While_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_while_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.match(AritmeticaParser.T__4)
            self.state = 45
            self.booleanexpression()
            self.state = 46
            self.match(AritmeticaParser.T__2)
            self.state = 48 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 47
                self.statement()
                self.state = 50 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__0) | (1 << AritmeticaParser.T__4) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 52
            self.match(AritmeticaParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def booleanexpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BooleanexpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_if_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_statement" ):
                listener.enterIf_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_statement" ):
                listener.exitIf_statement(self)




    def if_statement(self):

        localctx = AritmeticaParser.If_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_if_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            self.match(AritmeticaParser.T__5)
            self.state = 55
            self.booleanexpression()
            self.state = 56
            self.match(AritmeticaParser.T__2)
            self.state = 58 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 57
                self.statement()
                self.state = 60 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__0) | (1 << AritmeticaParser.T__4) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARIABLE) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 62
            self.match(AritmeticaParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(AritmeticaParser.VARIABLE, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_assign_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_statement" ):
                listener.enterAssign_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_statement" ):
                listener.exitAssign_statement(self)




    def assign_statement(self):

        localctx = AritmeticaParser.Assign_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_assign_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.match(AritmeticaParser.VARIABLE)
            self.state = 65
            self.match(AritmeticaParser.T__6)
            self.state = 66
            self.expression(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BooleanexpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,i)


        def COMPARATION_OPERATOR(self):
            return self.getToken(AritmeticaParser.COMPARATION_OPERATOR, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_booleanexpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBooleanexpression" ):
                listener.enterBooleanexpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBooleanexpression" ):
                listener.exitBooleanexpression(self)




    def booleanexpression(self):

        localctx = AritmeticaParser.BooleanexpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_booleanexpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.expression(0)
            self.state = 69
            self.match(AritmeticaParser.COMPARATION_OPERATOR)
            self.state = 70
            self.expression(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 14
        self.enterRecursionRule(localctx, 14, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self.term(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 83
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 81
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 75
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 76
                        self.match(AritmeticaParser.T__7)
                        self.state = 77
                        self.term(0)
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 78
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 79
                        self.match(AritmeticaParser.T__8)
                        self.state = 80
                        self.term(0)
                        pass

             
                self.state = 85
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 16
        self.enterRecursionRule(localctx, 16, self.RULE_term, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 87
            self.factor()
            self._ctx.stop = self._input.LT(-1)
            self.state = 94
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                    self.state = 89
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 90
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__9) | (1 << AritmeticaParser.T__10) | (1 << AritmeticaParser.T__11))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 91
                    self.factor() 
                self.state = 96
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.num = None # Token
            self.var = None # Token

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def VARIABLE(self):
            return self.getToken(AritmeticaParser.VARIABLE, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_factor)
        try:
            self.state = 103
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 97
                localctx.num = self.match(AritmeticaParser.NUMBER)
                pass
            elif token in [AritmeticaParser.VARIABLE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 98
                localctx.var = self.match(AritmeticaParser.VARIABLE)
                pass
            elif token in [AritmeticaParser.T__12]:
                self.enterOuterAlt(localctx, 3)
                self.state = 99
                self.match(AritmeticaParser.T__12)
                self.state = 100
                self.expression(0)
                self.state = 101
                self.match(AritmeticaParser.T__13)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[7] = self.expression_sempred
        self._predicates[8] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 1)
         




