# Generated from Seminario1.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\6")
        buf.write("\34\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\6\2\f\n\2\r\2")
        buf.write("\16\2\r\3\3\3\3\5\3\22\n\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5")
        buf.write("\3\5\3\5\2\2\6\2\4\6\b\2\2\2\31\2\13\3\2\2\2\4\21\3\2")
        buf.write("\2\2\6\23\3\2\2\2\b\27\3\2\2\2\n\f\5\4\3\2\13\n\3\2\2")
        buf.write("\2\f\r\3\2\2\2\r\13\3\2\2\2\r\16\3\2\2\2\16\3\3\2\2\2")
        buf.write("\17\22\5\6\4\2\20\22\5\b\5\2\21\17\3\2\2\2\21\20\3\2\2")
        buf.write("\2\22\5\3\2\2\2\23\24\7\3\2\2\24\25\7\5\2\2\25\26\b\4")
        buf.write("\1\2\26\7\3\2\2\2\27\30\7\4\2\2\30\31\7\5\2\2\31\32\b")
        buf.write("\5\1\2\32\t\3\2\2\2\4\r\21")
        return buf.getvalue()


class Seminario1Parser ( Parser ):

    grammarFileName = "Seminario1.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'saludar '", "'insultar '" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "NOMBRE", "GARBAGE" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_regla_saludar = 2
    RULE_reglar_insultar = 3

    ruleNames =  [ "program", "statement", "regla_saludar", "reglar_insultar" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    NOMBRE=3
    GARBAGE=4

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Seminario1Parser.StatementContext)
            else:
                return self.getTypedRuleContext(Seminario1Parser.StatementContext,i)


        def getRuleIndex(self):
            return Seminario1Parser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = Seminario1Parser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 9 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 8
                self.statement()
                self.state = 11 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==Seminario1Parser.T__0 or _la==Seminario1Parser.T__1):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def regla_saludar(self):
            return self.getTypedRuleContext(Seminario1Parser.Regla_saludarContext,0)


        def reglar_insultar(self):
            return self.getTypedRuleContext(Seminario1Parser.Reglar_insultarContext,0)


        def getRuleIndex(self):
            return Seminario1Parser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = Seminario1Parser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 15
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [Seminario1Parser.T__0]:
                self.enterOuterAlt(localctx, 1)
                self.state = 13
                self.regla_saludar()
                pass
            elif token in [Seminario1Parser.T__1]:
                self.enterOuterAlt(localctx, 2)
                self.state = 14
                self.reglar_insultar()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Regla_saludarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._NOMBRE = None # Token

        def NOMBRE(self):
            return self.getToken(Seminario1Parser.NOMBRE, 0)

        def getRuleIndex(self):
            return Seminario1Parser.RULE_regla_saludar

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRegla_saludar" ):
                listener.enterRegla_saludar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRegla_saludar" ):
                listener.exitRegla_saludar(self)




    def regla_saludar(self):

        localctx = Seminario1Parser.Regla_saludarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_regla_saludar)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            self.match(Seminario1Parser.T__0)
            self.state = 18
            localctx._NOMBRE = self.match(Seminario1Parser.NOMBRE)
            print('hola ' + (None if localctx._NOMBRE is None else localctx._NOMBRE.text)) 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Reglar_insultarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._NOMBRE = None # Token

        def NOMBRE(self):
            return self.getToken(Seminario1Parser.NOMBRE, 0)

        def getRuleIndex(self):
            return Seminario1Parser.RULE_reglar_insultar

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReglar_insultar" ):
                listener.enterReglar_insultar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReglar_insultar" ):
                listener.exitReglar_insultar(self)




    def reglar_insultar(self):

        localctx = Seminario1Parser.Reglar_insultarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_reglar_insultar)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21
            self.match(Seminario1Parser.T__1)
            self.state = 22
            localctx._NOMBRE = self.match(Seminario1Parser.NOMBRE)
            print('&/#% ' + (None if localctx._NOMBRE is None else localctx._NOMBRE.text)) 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





