#-*- coding: utf-8 -*-

import antlr4
from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream

from ConjuntosLexer import ConjuntosLexer
from ConjuntosParser import ConjuntosParser
from ConjuntosListener import ConjuntosListener

DOMAIN_CONSTANT = [0, 100]

class Visitador(ConjuntosListener):
    def __init__(self):
        self.variables = {}

    def enterProgram(self, ctx:ConjuntosParser.ProgramContext):
        for nodo in ctx.children:
            self.visitStatement(nodo)
    
    def visitStatement(self, ctx:ConjuntosParser.StatementContext):
        if type(ctx.children[0]) == ConjuntosParser.Show_setContext:
            self.showSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Sum_setContext:
            self.sumSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Add_setContext:
            self.addToSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Prom_setContext:
            self.promSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Remove_setContext:
            self.removeFromSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Union_setContext:
            self.unionSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Intersect_setContext:
            self.intersectSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Complement_setContext:
            self.complementSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Diff_setContext:
            self.diffSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.New_setContext:
            self.newSet(ctx.children[0])
        elif type(ctx.children[0]) == ConjuntosParser.Num_setContext:
            self.visitSet(ctx.children[0])

    def visitSet(self, ctx:ConjuntosParser.Num_setContext):
        value = []
        
        if ctx.var:
            value = self.variables[ctx.var.text]
        else:
            for num in ctx.children[0].symbol.text[::2]:
                value.append(int(num))

        return value

    def showSet(self, ctx:ConjuntosParser.Show_setContext):
        print("Show set: ",  end = '')
        print(','.join(self.visitSet(ctx.children[1])))

    def sumSet(self, ctx:ConjuntosParser.Sum_setContext):
        print("Sum set: ",  end = '')
        sum = 0
        for num in self.visitSet(ctx.children[1]):
            sum += int(num)
        print(sum)

    def promSet(self, ctx:ConjuntosParser.Prom_setContext):
        print("Prom..")
        sum = 0
        count = 0
        for num in self.visitSet(ctx.children[1]):
            sum += int(num)
            count += 1
        print(sum/count)

    def removeFromSet(self, ctx:ConjuntosParser.Remove_setContext):
        newSet = []
        numToDelete = int(ctx.children[2].symbol.text)
        base_set = self.visitSet(ctx.children[0])

        print("Rem num " + numToDelete + " from set: "+ ','.join(base_set))
        
        for num in base_set:
            if numToDelete != num:
                newSet.append(num)
        
        if ctx.children[0].var:
            self.variables[ctx.children[0].var.text] = newSet

        print('(' + ','.join(map(str, newSet))  + ')')

    def addToSet(self, ctx:ConjuntosParser.Add_setContext):
        base_set = []

        if ctx.children[0].var:
            #Get Set
            base_set = self.visitSet(ctx.children[0])
                
            #Add Number to set
            base_set.append(int(ctx.children[2].symbol.text))
            base_set.sort()

            self.variables[ctx.children[0].var.text] = base_set
        else:
            #Get Set
            for num in self.visitSet(ctx.children[0]):
                base_set.append(num)
            #Add Number to set
            base_set.append(int(ctx.children[2].symbol.text))
            base_set.sort()
        print("Result set: " + ','.join(base_set))

    def unionSet(self, ctx:ConjuntosParser.Union_setContext):
        base_set = []
        set_to_add = []

        #Get Sets
        for num in self.visitSet(ctx.children[0]):
            base_set.append(num)
        
        for num in self.visitSet(ctx.children[2]):
            set_to_add.append(num)

        print(','.join(base_set) + " UNION " + ','.join(set_to_add) + ": ")

        #Append Sets removing duplicates
        base_set = base_set + list(set(set_to_add) - set(base_set))
        base_set.sort()
        print(','.join(base_set))

    def intersectSet(self, ctx:ConjuntosParser.Intersect_setContext):
        base_set = []
        set_to_intersect_with = []

        #Get Sets
        for num in self.visitSet(ctx.children[0]):
            base_set.append(num)
        
        for num in self.visitSet(ctx.children[2]):
            set_to_intersect_with.append(num)

        print(','.join(base_set) + " INTERSECT " + ','.join(set_to_intersect_with) + ": ")

        #intersect Sets removing duplicates and sorting
        base_set = list(set(base_set) - set(set_to_intersect_with)) + list(set(set_to_intersect_with) - set(base_set))
        base_set.sort()
        print(','.join(base_set))
        
    def diffSet(self, ctx:ConjuntosParser.Diff_setContext):
        base_set = []
        set_to_diff_with = []

        #Get Sets
        for num in self.visitSet(ctx.children[0]):
            base_set.append(num)
        
        for num in self.visitSet(ctx.children[2]):
            set_to_diff_with.append(num)

        print(','.join(base_set) + " DIFF " + ','.join(set_to_diff_with) + ": ")

        #diff Sets removing duplicates and sorting
        base_set = list(set(base_set) - set(set_to_diff_with))
        base_set.sort()
        print(','.join(base_set))
        
    def complementSet(self, ctx:ConjuntosParser.Complement_setContext):
        complement = []
        base_set = []
        
        for num in self.visitSet(ctx.children[0]):
            base_set.append(int(num))

        for i in range(DOMAIN_CONSTANT[0], DOMAIN_CONSTANT[1]+1):
            if i not in base_set:
                complement.append(i)

        print('Complemento del dominio 0..100')
        print(complement)

    def newSet(self, ctx:ConjuntosParser.New_setContext):
        value = [] 
        for num in ctx.children[2].symbol.text[::2]:
            value.append(num);

        self.variables[ctx.children[0].symbol.text] = value

expr = '''
1,2,3 r 2
'''

print('Comenzando...')
input = InputStream(expr)
lexer = ConjuntosLexer(input)
stream = CommonTokenStream(lexer)
parser = ConjuntosParser(stream)

tree = parser.program()

nuestroListener = Visitador()
walker = ParseTreeWalker()
walker.walk(nuestroListener, tree)

print('Fin.')