#-*- coding: utf-8 -*-

import antlr4
from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream

from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener

class Visitador(AritmeticaListener):

    def __init__(self):
        self.stack = {}

    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        for nodo in ctx.children:
            self.visitStatement(nodo)

    def visitStatement(self, ctx:AritmeticaParser.StatementContext):
        

expr = '''
44
'''


print('Comenzando...')
input = InputStream(expr)
lexer = AritmeticaLexer(input)
stream = CommonTokenStream(lexer)
parser = AritmeticaParser(stream)

tree = parser.program()

nuestroListener = Visitador()
walker = ParseTreeWalker()
walker.walk(nuestroListener, tree)

print('Fin.')