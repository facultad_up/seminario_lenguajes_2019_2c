grammar Conjuntos;

program:
    (statement)+
    ;
    
statement:
    new_set
    | add_set
    | remove_set
    | union_set
    | intersect_set
    | complement_set
    | diff_set
    | show_set
    | prom_set
    | sum_set
    | num_set
    ;

new_set:
    VARIABLE '<-' SETLIST
    ;

add_set:
    num_set '+' numerito=NUMBER
    ;

remove_set:
    num_set 'r' NUMBER
    ;

union_set:
    num_set 'u' num_set
    ;

intersect_set:
    num_set 'n' num_set
    ;

complement_set:
    num_set '!'
    ;

diff_set:
    num_set 'd' num_set
    ;

show_set:
    'show ' num_set
    ;

sum_set:
    'sum ' num_set
    ;

prom_set:
    'prom ' num_set
    ;

num_set: 
    setlist=SETLIST
    | var=VARIABLE
    ;

VARIABLE: [a-zA-Z]+[a-zA-Z0-9]*;
SETLIST: NUMBER | NUMBER (',' NUMBER )+;
NUMBER : DIGIT+;
DIGIT  : [0-9];

WS : [ \r\n\t] -> skip;