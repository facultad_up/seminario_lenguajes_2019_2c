grammar JSON;

json :
  value
  ;

jsonobject :
    '{' pair (',' pair)*  '}'
  | '{' '}'
  ;

pair :
  p=STRING ':' value {print($p.text)}
  ;

array :
    '[' value (',' value)* ']'
  | '[' ']'
  ;

value :
    s=STRING  {print($s.text) }
  | n=NUMERO  {print($n.text) }
  | jsonobject
  | 'true'    {print('true')}
  | 'false'   {print('false')}
  | 'null'
  | array
  ;

STRING : '"' [0-9a-zA-Z]+ '"';
NUMERO : [0-9]+;

WS : [ \r\n\t] -> skip;
