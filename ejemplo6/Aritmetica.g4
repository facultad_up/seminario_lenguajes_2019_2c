grammar Aritmetica;

statement:
    e=expression
    ;

expression returns [int value]: 
      t=term
    | e=expression '+' t=term
    | e=expression '-' t=term
    ;

term returns [int value]:
      f=factor
    | t=term '*' f=factor
    | t=term '/' f=factor
    | t=term '^' f=factor
    ;

factor returns [int value]:
      NUMBER
    | '(' e=expression ')'
    ;

NUMBER : DIGIT+;
DIGIT  : [0-9];

WS : [ \r\n\t] -> skip;
