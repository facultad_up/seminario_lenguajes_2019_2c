# Generated from Conjuntos.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\21")
        buf.write("W\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t")
        buf.write("\16\3\2\6\2\36\n\2\r\2\16\2\37\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\5\3-\n\3\3\4\3\4\3\4\3\4\3\5\3\5")
        buf.write("\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3")
        buf.write("\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f")
        buf.write("\3\f\3\r\3\r\3\r\3\16\3\16\5\16U\n\16\3\16\2\2\17\2\4")
        buf.write("\6\b\n\f\16\20\22\24\26\30\32\2\2\2U\2\35\3\2\2\2\4,\3")
        buf.write("\2\2\2\6.\3\2\2\2\b\62\3\2\2\2\n\66\3\2\2\2\f:\3\2\2\2")
        buf.write("\16>\3\2\2\2\20B\3\2\2\2\22E\3\2\2\2\24I\3\2\2\2\26L\3")
        buf.write("\2\2\2\30O\3\2\2\2\32T\3\2\2\2\34\36\5\4\3\2\35\34\3\2")
        buf.write("\2\2\36\37\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2 \3\3\2\2")
        buf.write("\2!-\5\6\4\2\"-\5\b\5\2#-\5\n\6\2$-\5\f\7\2%-\5\16\b\2")
        buf.write("&-\5\20\t\2\'-\5\22\n\2(-\5\24\13\2)-\5\30\r\2*-\5\26")
        buf.write("\f\2+-\5\32\16\2,!\3\2\2\2,\"\3\2\2\2,#\3\2\2\2,$\3\2")
        buf.write("\2\2,%\3\2\2\2,&\3\2\2\2,\'\3\2\2\2,(\3\2\2\2,)\3\2\2")
        buf.write("\2,*\3\2\2\2,+\3\2\2\2-\5\3\2\2\2./\7\r\2\2/\60\7\3\2")
        buf.write("\2\60\61\7\16\2\2\61\7\3\2\2\2\62\63\5\32\16\2\63\64\7")
        buf.write("\4\2\2\64\65\7\17\2\2\65\t\3\2\2\2\66\67\5\32\16\2\67")
        buf.write("8\7\5\2\289\7\17\2\29\13\3\2\2\2:;\5\32\16\2;<\7\6\2\2")
        buf.write("<=\5\32\16\2=\r\3\2\2\2>?\5\32\16\2?@\7\7\2\2@A\5\32\16")
        buf.write("\2A\17\3\2\2\2BC\5\32\16\2CD\7\b\2\2D\21\3\2\2\2EF\5\32")
        buf.write("\16\2FG\7\t\2\2GH\5\32\16\2H\23\3\2\2\2IJ\7\n\2\2JK\5")
        buf.write("\32\16\2K\25\3\2\2\2LM\7\13\2\2MN\5\32\16\2N\27\3\2\2")
        buf.write("\2OP\7\f\2\2PQ\5\32\16\2Q\31\3\2\2\2RU\7\16\2\2SU\7\r")
        buf.write("\2\2TR\3\2\2\2TS\3\2\2\2U\33\3\2\2\2\5\37,T")
        return buf.getvalue()


class ConjuntosParser ( Parser ):

    grammarFileName = "Conjuntos.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'<-'", "'+'", "'r'", "'u'", "'n'", "'!'", 
                     "'d'", "'show '", "'sum '", "'prom '" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "VARIABLE", 
                      "SETLIST", "NUMBER", "DIGIT", "WS" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_new_set = 2
    RULE_add_set = 3
    RULE_remove_set = 4
    RULE_union_set = 5
    RULE_intersect_set = 6
    RULE_complement_set = 7
    RULE_diff_set = 8
    RULE_show_set = 9
    RULE_sum_set = 10
    RULE_prom_set = 11
    RULE_num_set = 12

    ruleNames =  [ "program", "statement", "new_set", "add_set", "remove_set", 
                   "union_set", "intersect_set", "complement_set", "diff_set", 
                   "show_set", "sum_set", "prom_set", "num_set" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    VARIABLE=11
    SETLIST=12
    NUMBER=13
    DIGIT=14
    WS=15

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ConjuntosParser.StatementContext)
            else:
                return self.getTypedRuleContext(ConjuntosParser.StatementContext,i)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = ConjuntosParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 27 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 26
                self.statement()
                self.state = 29 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << ConjuntosParser.T__7) | (1 << ConjuntosParser.T__8) | (1 << ConjuntosParser.T__9) | (1 << ConjuntosParser.VARIABLE) | (1 << ConjuntosParser.SETLIST))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def new_set(self):
            return self.getTypedRuleContext(ConjuntosParser.New_setContext,0)


        def add_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Add_setContext,0)


        def remove_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Remove_setContext,0)


        def union_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Union_setContext,0)


        def intersect_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Intersect_setContext,0)


        def complement_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Complement_setContext,0)


        def diff_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Diff_setContext,0)


        def show_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Show_setContext,0)


        def prom_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Prom_setContext,0)


        def sum_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Sum_setContext,0)


        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = ConjuntosParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 42
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 31
                self.new_set()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 32
                self.add_set()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 33
                self.remove_set()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 34
                self.union_set()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 35
                self.intersect_set()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 36
                self.complement_set()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 37
                self.diff_set()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 38
                self.show_set()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 39
                self.prom_set()
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 40
                self.sum_set()
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 41
                self.num_set()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class New_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE(self):
            return self.getToken(ConjuntosParser.VARIABLE, 0)

        def SETLIST(self):
            return self.getToken(ConjuntosParser.SETLIST, 0)

        def getRuleIndex(self):
            return ConjuntosParser.RULE_new_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNew_set" ):
                listener.enterNew_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNew_set" ):
                listener.exitNew_set(self)




    def new_set(self):

        localctx = ConjuntosParser.New_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_new_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.match(ConjuntosParser.VARIABLE)
            self.state = 45
            self.match(ConjuntosParser.T__0)
            self.state = 46
            self.match(ConjuntosParser.SETLIST)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Add_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.numerito = None # Token

        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def NUMBER(self):
            return self.getToken(ConjuntosParser.NUMBER, 0)

        def getRuleIndex(self):
            return ConjuntosParser.RULE_add_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdd_set" ):
                listener.enterAdd_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdd_set" ):
                listener.exitAdd_set(self)




    def add_set(self):

        localctx = ConjuntosParser.Add_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_add_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.num_set()
            self.state = 49
            self.match(ConjuntosParser.T__1)
            self.state = 50
            localctx.numerito = self.match(ConjuntosParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Remove_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def NUMBER(self):
            return self.getToken(ConjuntosParser.NUMBER, 0)

        def getRuleIndex(self):
            return ConjuntosParser.RULE_remove_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRemove_set" ):
                listener.enterRemove_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRemove_set" ):
                listener.exitRemove_set(self)




    def remove_set(self):

        localctx = ConjuntosParser.Remove_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_remove_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.num_set()
            self.state = 53
            self.match(ConjuntosParser.T__2)
            self.state = 54
            self.match(ConjuntosParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Union_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ConjuntosParser.Num_setContext)
            else:
                return self.getTypedRuleContext(ConjuntosParser.Num_setContext,i)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_union_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnion_set" ):
                listener.enterUnion_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnion_set" ):
                listener.exitUnion_set(self)




    def union_set(self):

        localctx = ConjuntosParser.Union_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_union_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 56
            self.num_set()
            self.state = 57
            self.match(ConjuntosParser.T__3)
            self.state = 58
            self.num_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Intersect_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ConjuntosParser.Num_setContext)
            else:
                return self.getTypedRuleContext(ConjuntosParser.Num_setContext,i)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_intersect_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIntersect_set" ):
                listener.enterIntersect_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIntersect_set" ):
                listener.exitIntersect_set(self)




    def intersect_set(self):

        localctx = ConjuntosParser.Intersect_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_intersect_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 60
            self.num_set()
            self.state = 61
            self.match(ConjuntosParser.T__4)
            self.state = 62
            self.num_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Complement_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_complement_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComplement_set" ):
                listener.enterComplement_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComplement_set" ):
                listener.exitComplement_set(self)




    def complement_set(self):

        localctx = ConjuntosParser.Complement_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_complement_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.num_set()
            self.state = 65
            self.match(ConjuntosParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Diff_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ConjuntosParser.Num_setContext)
            else:
                return self.getTypedRuleContext(ConjuntosParser.Num_setContext,i)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_diff_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDiff_set" ):
                listener.enterDiff_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDiff_set" ):
                listener.exitDiff_set(self)




    def diff_set(self):

        localctx = ConjuntosParser.Diff_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_diff_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 67
            self.num_set()
            self.state = 68
            self.match(ConjuntosParser.T__6)
            self.state = 69
            self.num_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Show_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_show_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterShow_set" ):
                listener.enterShow_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitShow_set" ):
                listener.exitShow_set(self)




    def show_set(self):

        localctx = ConjuntosParser.Show_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_show_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 71
            self.match(ConjuntosParser.T__7)
            self.state = 72
            self.num_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sum_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_sum_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSum_set" ):
                listener.enterSum_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSum_set" ):
                listener.exitSum_set(self)




    def sum_set(self):

        localctx = ConjuntosParser.Sum_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_sum_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.match(ConjuntosParser.T__8)
            self.state = 75
            self.num_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Prom_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def num_set(self):
            return self.getTypedRuleContext(ConjuntosParser.Num_setContext,0)


        def getRuleIndex(self):
            return ConjuntosParser.RULE_prom_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProm_set" ):
                listener.enterProm_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProm_set" ):
                listener.exitProm_set(self)




    def prom_set(self):

        localctx = ConjuntosParser.Prom_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_prom_set)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 77
            self.match(ConjuntosParser.T__9)
            self.state = 78
            self.num_set()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Num_setContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.setlist = None # Token
            self.var = None # Token

        def SETLIST(self):
            return self.getToken(ConjuntosParser.SETLIST, 0)

        def VARIABLE(self):
            return self.getToken(ConjuntosParser.VARIABLE, 0)

        def getRuleIndex(self):
            return ConjuntosParser.RULE_num_set

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNum_set" ):
                listener.enterNum_set(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNum_set" ):
                listener.exitNum_set(self)




    def num_set(self):

        localctx = ConjuntosParser.Num_setContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_num_set)
        try:
            self.state = 82
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [ConjuntosParser.SETLIST]:
                self.enterOuterAlt(localctx, 1)
                self.state = 80
                localctx.setlist = self.match(ConjuntosParser.SETLIST)
                pass
            elif token in [ConjuntosParser.VARIABLE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 81
                localctx.var = self.match(ConjuntosParser.VARIABLE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





